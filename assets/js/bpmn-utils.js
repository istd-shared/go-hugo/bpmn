/**
 * 
 * @param {string} name Element Business Object Name
 * @param {string} sectionUrl Publication Section URL
 */
function businessObjectRouter(name, sectionUrl) {
    const pageUrl = sectionUrl + '/' + name.toLowerCase().replaceAll(' ', '-');
    console.log('businessObjectRouter => ' + pageUrl);
    window.location.href = pageUrl;
}