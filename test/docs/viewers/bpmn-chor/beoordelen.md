---
bookToC: false
weight: 1
---

# Choreografie Viewer

BPMN weergave vanuit bestand [wmo_beoordelen.bpmn](wmo_beoordelen.bpmn).

{{< bpmn-chor-viewer file="beoordelen.bpmn" name="WMO beoordelen" height=600 width=1000 >}}
